Rechecker is for the most part a generic endpoint checker. You could swap out the dbs used or endpoints checked  base on your need, however the idea is still the same.

This is version two of the rechecker app now in RestfulAPI form. It's been awhile since I looked through this code but attaching an image of the servie map to at least give some idea of what's going on.

As time allows I'm moving towards rewriting this with FastAPI.

In this set up this app was first built locally with docker, and then pushed to ECR.

The ecr script simplified this process and was placed in each rechecker directory to build each image, however I have moved it to the scripts directory for simplicity

The start up script is used in both of scheduling microservices to trigger the others on a timer. Currently that timer is every 8mins for healthcheck sync, and 15mins for db sync

The third version of this application I'm planning on adding async to significatly speed up the throughput, which is why I'll be moving to FastAPI instead of manually creating them with Flask

Service Map for Version 2 of rechecker

![Alt text](service-map.png?raw=true "Service Map V2")
