from flask import Flask, Response, request, jsonify, abort
import datetime
import dns
import dns.resolver
import json
import requests
from elasticsearch import Elasticsearch, RequestsHttpConnection

# set system variables
SERVER_NAME = "API/0.1.0"
#ABSTRACT_API_KEY = os.environ["ABSTRACT_API_KEY"]

# class to hide http headers
class localFlask(Flask):
    def process_response(self, response):
        response.headers["server"] = SERVER_NAME
        return response

# set flask variables
app = localFlask(__name__)

es = Elasticsearch(['https://kibana.rechecker.local:443'])
time = datetime.datetime.now()
#time = time_raw.strftime("%b %d, %Y " "@" " %H:%M:%S.%f")[:-3]
date = datetime.date.today()
index = ("rechecker-" + str(date))

@app.route("/", methods=["POST"])
def dns_checker():
    #print('I check DNS status')
    raw_json = request.get_json
    site = raw_json('domain')
    try:
        call = dns.resolver.resolve(site, 'A')
        for ipval in call:
            return str(200)
    except dns.resolver.NXDOMAIN:
        dns_check_response = ({
                    "domain": f"{site}",
                    "status": "not found",
                    "timestamp": f"{time}"
            })
        res = es.index(index=index, body=http_checker_response)
        print(res['result'], flush=True)
        print(json.dumps(dns_check_response), flush=True)
        return ''
    except dns.resolver.NoAnswer:
        dns_check_response = ({
                    "domain": f"{site}",
                    "status": "no reply",
                    "timestamp": f"{time}"
            })
        res = es.index(index=index, body=http_checker_response)
        print(res['result'], flush=True)
        print(json.dumps(dns_check_response), flush=True)
        return ''
    except dns.exception.Timeout:
        dns_check_response = ({
                    "domain": f"{site}",
                    "status": "no reply",
                    "timestamp": f"{time}"
            })
        res = es.index(index=index, body=http_checker_response)
        print(res['result'], flush=True)
        print(json.dumps(dns_check_response), flush=True)
        return ''

if __name__ == "__main__":
    app.run(debug=True, host='0.0.0.0', port='5000')