import dns
import json
import requests
def healthcheck_sync():
    print('I schedule the healthcheck')
    domains = json.loads(requests.get("http://recheck-endpoint-service:5000").text)
    for raw_json in domains:
        try:
            requests.post("http://recheck-http-service:5000", json=raw_json )
            requests.post("http://recheck-dns-service:5000", json=raw_json )
        except:
            post_failed = ({
                    "_source": {
                        "message": f"{site}",
                        "status": "failed to post"
                    }
                })
            print(jsonify(post_failed))
            pass
    print('ok', flush=True)
healthcheck_sync()