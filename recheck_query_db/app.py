from flask import Flask, Response, request, jsonify, abort
import dns
import json
import mysql.connector
from mysql.connector import Error

# set system variables
SERVER_NAME = "API/0.1.0"
#ABSTRACT_API_KEY = os.environ["ABSTRACT_API_KEY"]

# class to hide http headers
class localFlask(Flask):
    def process_response(self, response):
        response.headers["server"] = SERVER_NAME
        return response

app = localFlask(__name__)

@app.route("/", methods=["GET"])
def query_db():
    print("I serve url_redirection_base table from Maria")
    domains = []
    try:
        connection = mysql.connector.connect(host='mariadb.rechecker.local',
                                             database='database',
                                             user='user',
                                             password='password')

        sql_select_Query = "select * from table where source_id='1' and status='active'"
        cursor = connection.cursor()
        cursor.execute(sql_select_Query)
        records = cursor.fetchall()
        for sites in records:
            site = sites[1]
            print(site, flush=True)
            domains.append({  
                'domain': site 
                })
        print(domains, flush=True)
        return jsonify(domains), 200

    except Error as e:
        print("Error reading data from MySQL table", e, flush=True)
        return ("Error reading data from MySQL table"), 401
    finally:
        if (connection.is_connected()):
            connection.close()
            cursor.close()
            print("MySQL connection is closed", flush=True)
    
if __name__ == "__main__":
    app.run(debug=True, host='0.0.0.0', port='5000')