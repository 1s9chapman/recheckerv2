from flask import Flask, Response, request, jsonify, abort
from elasticsearch import Elasticsearch, RequestsHttpConnection
import dns
import json
import pymongo
from pymongo import MongoClient

# set system variables
SERVER_NAME = "API/0.1.0"
#ABSTRACT_API_KEY = os.environ["ABSTRACT_API_KEY"]

# class to hide http headers
class localFlask(Flask):
    def process_response(self, response):
        response.headers["server"] = SERVER_NAME
        return response

app = localFlask(__name__)

#domains = {}

#host = 'https://kibana.rechecker.local/'
#es = Elasticsearch(hosts=[{'host': host, 'port': 443}],
#    use_ssl=True,
#    verify_certs=False,
#    connection_class=RequestsHttpConnection)
#print(es.info())

@app.route("/", methods=["GET"])
def get_endpoints():
    print('I provide a list of endpoints')
    domains = []
    try:
        # The uri format is mongodb+srv://<user_name>:<password>@<cluster_name>/<collection_name>?retryWrites=true&w=majority
        client = MongoClient("mongodb+srv://user:password@cluster.id.mongodb.net/host?retryWrites=true&w=majority")
        # .hosts refers to the database to connect to
        db = client.endpoints
        # This for look loop iterates over each entry in the database.
        # These are json entries with name: "domain.com" key value pairs
        # .sites is the Collection in the .hosts database
        #domains = db.sites.find()
        for site in db.sites.find():
            #print(site['domain'])
            domains.append(site['domain'])
            #print(site['domain'])
            #pass jsonify(domains), 200
            #res = es.index(index="test-index", id=1, body=domains)
            #print(res['result'])
        #print(dict(domains), flush=True)
        return jsonify(domains), 200
    # in case URI syntax is incorrect
    except pymongo.errors.ConfigurationError:
        print('Please check the syntax of your cluster in MongoClient', flush=True)
        return abort(404)
    except pymongo.errors.InvalidURI:
        print('Please check the syntax of your URI in MongoClient', flush=True)
        return abort(401)
    # in case username or password is incorrect
    except pymongo.errors.OperationFailure:
        print('Check your username and password', flush=True)
        return abort(401)

if __name__ == "__main__":
    app.run(debug=True, host='0.0.0.0', port='5000')