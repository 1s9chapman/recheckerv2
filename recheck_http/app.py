from flask import Flask, Response, request, jsonify, abort
from elasticsearch import Elasticsearch, RequestsHttpConnection
import datetime
import dns
import json
import requests

# set system variables
SERVER_NAME = "API/0.1.0"
#ABSTRACT_API_KEY = os.environ["ABSTRACT_API_KEY"]

# class to hide http headers
class localFlask(Flask):
    def process_response(self, response):
        response.headers["server"] = SERVER_NAME
        return response

app = localFlask(__name__)

es = Elasticsearch(['https://kibana.rechecker.local:443'])
time = datetime.datetime.now()
#time = time_raw.strftime("%b %d, %Y " "@" " %H:%M:%S.%f")[:-3]
date = datetime.date.today()
index = ("rechecker-" + str(date))

@app.route("/", methods=["POST"])
def http_checker():
    #print('I check http status')
    raw_json = request.get_json
    site = raw_json('domain')
    try:
        call = requests.get('http://' + site, timeout=1)
        code = str(call.status_code)
        if call.status_code == 200 and 302:
            return ''
        else:
            http_checker_response = {
                        "domain": f"{site}",
                        "code": f"{code}",
                        "timestamp": f"{time}"
            }
            res = es.index(index=index, body=http_checker_response)
            print(res['result'], flush=True)
            print(json.dumps(http_checker_response), flush=True)
            return ''
    except requests.ConnectionError:
        code = str(500)
        http_checker_response = {
                        "domain": f"{site} connection error",
                        "code": f"{code}",
                        "timestamp": f"{time}"
            }
        res = es.index(index=index, body=http_checker_response)
        print(res['result'], flush=True)
        print(json.dumps(http_checker_response), flush=True)
        return ''
    except requests.exceptions.ReadTimeout:
        code = str(500)
        http_checker_response = {
                        "domain": f"{site} timeout",
                        "code": f"{code}",
                        "timestamp": f"{time}"
            }
        res = es.index(index=index, body=http_checker_response)
        print(res['result'], flush=True)
        print(json.dumps(http_checker_response), flush=True)
        return ''

if __name__ == "__main__":
    app.run(debug=True, host='0.0.0.0', port='5000')