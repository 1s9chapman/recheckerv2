from flask import Flask, Response, request, jsonify, abort
import dns
import json
import pymongo
from pymongo import MongoClient
import requests
from urllib.parse import urlparse

# set system variables
SERVER_NAME = "API/0.1.0"
# ABSTRACT_API_KEY = os.environ["ABSTRACT_API_KEY"]

# class to hide http headers
class localFlask(Flask):
    def process_response(self, response):
        response.headers["server"] = SERVER_NAME
        return response

app = localFlask(__name__)

@app.route("/", methods=["GET"])
def update_db():
    print("I update MongoDB from Maria")
    domains = json.loads(requests.get("http://recheck-query-db-service:5000").text)
    for domain in domains:
        try:
            client = MongoClient("mongodb+srv://user:password@cluster.id.mongodb.net/host?retryWrites=true&w=majority")
            db = client.endpoints
            collection = db.sites
            site = urlparse(domain["domain"])
            print(site, flush=True)
            doc = {"domain": site.netloc}
            print(doc, flush=True)
            upsert_result = collection.update_one( doc, { "$set": doc }, upsert=True )
            print(upsert_result.raw_result, flush=True)
        except pymongo.errors.ConfigurationError:
            print('Please check the syntax of your cluster in MongoClient', flush=True)
            return abort(404)
        except pymongo.errors.InvalidURI:
            print('Please check the syntax of your URI in MongoClient', flush=True)
            return abort(401)
            # in case username or password is incorrect
        except pymongo.errors.OperationFailure:
            print('Check your username and password', flush=True)
            return abort(401)
    return jsonify({"db updated": "ok"}), 200
    
if __name__ == "__main__":
    app.run(debug=True, host='0.0.0.0', port='5000')