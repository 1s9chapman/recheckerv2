#!/bin/bash
aws ecr get-login-password --region us-east-1 | docker login --username AWS --password-stdin {aws-account-id}.dkr.ecr.us-east-1.amazonaws.com
docker build -t $1 .
docker tag $1:latest {aws-account-id}.dkr.ecr.us-east-1.amazonaws.com/$1:latest
docker push {aws-account-id}.dkr.ecr.us-east-1.amazonaws.com/$1:latest
